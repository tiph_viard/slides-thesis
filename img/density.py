from Drawing import *

s = Drawing(alpha=0, omega=10)

s.addNode("a")
s.addNode("b")
s.addNode("c")

s.addLink("a", "b", 0, 5)
s.addLink("a", "c", 6, 7, height=0.40)
s.addLink("b", "c", 8, 10)


s.addTimeLine(ticks=2)