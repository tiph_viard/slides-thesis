import sys

class Drawing:

	alpha = 0
	omega = 0

	time_unit = 500
	node_unit = 600
	offset_x = 3450
	offset_y = 2250

	nodes = {}
	node_cpt = 1

	colors = {}
	color_cpt = 31


	def __init__(self, alpha=0, omega=10):
		print("""#FIG 3.2  Produced by xfig version 3.2.5b\n\
Landscape\n\
Center\n\
Inches\n\
Letter\n\
100.00\n\
Single\n\
-2\n\
1200 2\n""")

		self.alpha = alpha
		self.omega = omega

		self.linetype = 2

	def setLineType(def_linetype):
		self.linetype = def_linetype

	def addColor(self, name, hex):
		self.color_cpt += 1
		self.colors[name] = self.color_cpt

		print("0 " + str(self.color_cpt) + " " + str(hex))

	def addNode(self, u, times=[], color=0, linetype=None, width=2):
		""" nodeId : identifiant du noeud
			times : suite d'intervalles de temps ou le noeud est actif
		"""

		if linetype is None:
			linetype = self.linetype

		if color in self.colors:
			color = self.colors[color]

		self.node_cpt += 1
		self.nodes[u] = self.node_cpt

		print("4 0 " + str(color) + " 50 -1 0 30 0.0000 4 135 120 3000 " + str(self.offset_y + 125 + int(self.node_cpt * self.node_unit)) + " " + str(u) + "\\001")

		if len(times) == 0:
			print("""2 1 """ + str(linetype) + """ 2 """ + str(color) + """ 7 50 -1 -1 6.000 0 0 7 0 0 2\n \
	 	 """ + str(self.offset_x + self.alpha * self.time_unit) + """ """ + str(self.offset_y + int(self.node_cpt * self.node_unit)) + """ """ + str(self.offset_x + int(self.omega * self.time_unit)) + """ """ + str(self.offset_y + int(self.node_cpt * self.node_unit)))
		else:
			for (i,j) in times:
				print("""2 1 """ + str(linetype) + """ 2 """ + str(color) + """ 7 50 -1 -1 6.000 0 0 7 0 0 2\n \
	 	 """ + str(self.offset_x + int(i* self.time_unit)) + """ """ + str(self.offset_y + int(self.node_cpt * self.node_unit)) + """ """ + str(self.offset_x + int(j * self.time_unit)) + """ """ + str(self.offset_y + int(self.node_cpt * self.node_unit)))

	def addLink(self, u, v, b, e, color=0, height=0.5, width=3):
		
		if color in self.colors:
			color = self.colors[color]
		if self.nodes[u] > self.nodes[v]:
			(u,v) = (v,u)

		# Draw circles for u and v
		print("1 3 0 " + str(width) + " " + str(color) + " " + str(color) + " 49 -1 20 0.000 1 0.0000 " + str(self.offset_x + int(b * self.time_unit)) + " " + str(self.offset_y + self.nodes[u]*self.node_unit) + " 45 45 -6525 -2025 -6480 -2025")
		print("1 3 0 " + str(width) + " " + str(color) + " " + str(color) + " 49 -1 20 0.000 1 0.0000 " + str(self.offset_x + int(b * self.time_unit)) + " " + str(self.offset_y + self.nodes[v]*self.node_unit) + " 45 45 -6525 -2025 -6480 -2025")
		
		# Link them
		print("2 1 0 "+ str(width) + " " + str(color) + " 7 50 -1 -1 0.000 0 0 -1 0 0 2\n")
		print(str(self.offset_x + int(b * self.time_unit)) + " " + str(self.offset_y + self.nodes[u]*self.node_unit) + " " + str(self.offset_x + int(b * self.time_unit)) + " " + str(self.offset_y + self.nodes[v]*self.node_unit))

		numnodes = abs(self.nodes[u] - self.nodes[v])

		# Add duration
		print("2 1 0 " + str(width) + " " + str(color) + " 7 50 -1 -1 0.000 0 0 -1 0 0 2\n")
		print(str(self.offset_x + int(b * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit + (numnodes*self.node_unit*height))) + " " + str(self.offset_x + int(e * self.time_unit)) + " " + str(self.offset_y + self.nodes[v]*self.node_unit - (numnodes*self.node_unit*(1-height))) + "\n")

	def addNodeCluster(self, u, times=[], color=0, width=200):

		margin = int(width / 2)

		if color in self.colors:
			color = self.colors[color]		

		if len(times) == 0:
			print("2 2 0 0 0 " + str(color) + " 51 -1 20 0.000 0 0 -1 0 0 5\n")
			print(str(self.offset_x + int(self.alpha * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + " " + str(self.offset_x + int(self.omega * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + " " + str(self.offset_x + int(self.omega * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) + margin) + " " + str(self.offset_x + int(self.alpha * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) + margin) + " " + str(self.offset_x + int(self.alpha * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + "\n")
		else:
			for (i,j) in times:
				print("2 2 0 0 0 " + str(color) + " 51 -1 20 0.000 0 0 -1 0 0 5\n")
				print(str(self.offset_x + int(i * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + " " + str(self.offset_x + int(j * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + " " + str(self.offset_x + int(j * self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) + margin) + " " + str(self.offset_x + int(i*self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) + margin) + " " + str(self.offset_x + int(i*self.time_unit)) + " " + str(self.offset_y + int(self.nodes[u]*self.node_unit) - margin) + "\n")

	def addPath(self, path, start, end, gamma=0, color=0, width=1):

		if color in self.colors:
			color = self.colors[color]
		
		last_time = self.alpha

		for (t,u,v) in path:
			# Wait on node ...
			print("2 1 0 " + str(width) + " " + str(color) + " 7 48 -1 -1 0.000 0 0 -1 0 0 2\n")
			print(str(self.offset_x + last_time * self.time_unit) + " " + str(self.offset_y + self.nodes[u] * self.node_unit) + " " + str(self.offset_x + t * self.time_unit) + " " + str(self.offset_y + self.nodes[u] * self.node_unit) + "\n")

			# Jump !
			print("2 1 0 " + str(width) + " " + str(color) + " 7 48 -1 -1 0.000 0 0 -1 0 0 2\n")
			print(str(self.offset_x + int(t * self.time_unit)) + " " + str(self.offset_y + self.nodes[u] * self.node_unit) + " " + str(self.offset_x + int((t + gamma) * self.time_unit)) + " " + str(self.offset_y + self.nodes[v] * self.node_unit) + "\n")

			last_time = t + gamma

		print("2 1 0 " + str(width) + " " + str(color) + " 7 48 -1 -1 0.000 0 0 -1 0 0 2\n")
		print(str(self.offset_x + last_time * self.time_unit) + " " + str(self.offset_y + self.nodes[v] * self.node_unit) + " " + str(self.offset_x + end * self.time_unit) + " " + str(self.offset_y + self.nodes[v] * self.node_unit) + "\n")

	def addTimeLine(self, ticks=1):
		timeline_y = self.node_cpt * self.node_unit + int(self.node_unit / 2)

		# Time arrow
		print("2 1 0 1 0 7 50 -1 -1 0.000 0 0 -1 1 0 2\n")
		print("1 1 1.00 60.00 120.00\n")
		print(str(self.offset_x + self.alpha * self.time_unit) + " " + str(self.offset_y + timeline_y) + " " + str(self.offset_x + self.omega * self.time_unit) + " " + str(self.offset_y + timeline_y) + "\n")

		# Time ticks
		for i in range(self.alpha, self.omega, ticks):
			print("2 1 0 1 0 7 50 -1 -1 0.000 0 0 -1 0 0 2\n")
			print(str(self.offset_x + int(i * self.time_unit)) + " " + str(self.offset_y + timeline_y) + " " + str(self.offset_x + int(i * self.time_unit)) + " " + str(self.offset_y + timeline_y + 30) + "\n")
			print("4 0 0 50 -1 0 20 0.0000 4 135 120 " + str(self.offset_x + int(i * self.time_unit) - 50) + " " + str(self.offset_y + timeline_y + 250) + " " + str(i) + "\\001\n")

		# Write "time"
		print("4 2 0 50 -1 0 18 0.0000 4 135 120 " + str(self.offset_x + int(self.omega * self.time_unit)) + " " + str(self.offset_y + timeline_y + 400) + " temps\\001\n")


# main
if __name__ == '__main__':

	s = Drawing()
	# s = Drawing(alpha=0, omega=100)

	s.addColor("grey", "#888888")
	s.addColor("red", "#ff0000")


	s.addNode("u", [(0,3.5), (8,10)])
	s.addNode("v")
	s.addNode("x")

	s.addNodeCluster("u", [(0,2)], color=11, width=200)
	s.addNodeCluster("v", color=11)
	s.addNodeCluster("u", [(8.5,9.5)], color="red")

	s.addLink("u", "v", 0.5, 2)
	s.addLink("u", "v", 1, 4, height=0.25)
	s.addLink("u", "v", 8, 9.5)
	s.addLink("v", "x", 3, 4.8)

	s.addPath([(1, "u", "v"),(4, "v", "x"), (8, "x", "u")], 0, 10, 1, color="red", width=2)

	s.addTimeLine(ticks=2)