from Drawing import *

s = Drawing(alpha=0, omega=25)

s.addNode("a")
s.addNode("b")
s.addNode("c", color=16)
s.addNode("d")
s.addNode("e")

s.addLink("a", "b", 1, 7)
s.addLink("a", "b", 9, 12)
s.addLink("b", "c", 3, 6)
s.addLink("b", "d", 7, 8, height=0.25)
s.addLink("c", "e", 10, 14, height=0.25)
s.addLink("d", "e", 1, 6)
s.addLink("c","d", 15, 20)
s.addLink("a","c", 16, 20, height=0.25)

s.addNodeCluster("b", [(3,6)], color=16)
s.addNodeCluster("e", [(10,14)], color=16)
s.addNodeCluster("d", [(15,20)], color=16)
s.addNodeCluster("a", [(16,20)], color=16)

s.addTimeLine(ticks=5)