from Drawing import *

s = Drawing(alpha=0, omega=10)

s.addColor("grey", "#bbbbbb")

s.addNode("a")
s.addNode("b")
# s.addNode("b", color=16)
s.addNode("c")

s.addLink("a", "b", 1, 6)
# s.addLink("a", "b", 1, 6, color="grey")
s.addLink("b", "c", 3, 7)
# s.addLink("b", "c", 3, 7, color="grey")
# s.addLink("a", "c", 2, 8, height=0.45) # Ex 1 base
s.addLink("a", "c", 5, 8, height=0.45) # Ex 2 base

# s.addNodeCluster("a", [(1,6)], color=16)
# s.addNodeCluster("c", [(3,7)], color=16)

s.addTimeLine()
