from Drawing import *

s = Drawing(alpha=0, omega=25)

s.addColor("red", "#FF6347")
s.addColor("green", "#32CD32")
s.addColor("blue", "#4169E1")

s.addNode("a")
s.addNode("b")
s.addNode("c")
s.addNode("d")
s.addNode("e")

s.addLink("a", "b", 1, 7)
# s.addLink("a", "b", 5, 7, color=15, width=4)
s.addLink("a", "b", 9, 12) # , color=15, width=4)
s.addLink("b", "c", 3, 6)
s.addLink("b", "c", 5, 6) # , color=15, width=4)
s.addLink("b", "d", 7, 8, height=0.25) # , color=15, width=4)
s.addLink("c", "e", 10, 14, height=0.25)
s.addLink("c", "e", 10, 12, height=0.25) # , color=15, width=4)
s.addLink("d", "e", 1, 6)
# s.addLink("d", "e", 5, 6, color=15, width=4)
s.addLink("c","d", 15, 20)
s.addLink("a","c", 16, 20, height=0.25)

# s.addNodeCluster("a", [(5,12)], color=16)
# s.addNodeCluster("b", [(5,12)], color=16)
# s.addNodeCluster("c", [(5,12)], color=16)
# s.addNodeCluster("d", [(5,12)], color=16)
# s.addNodeCluster("e", [(5,12)], color=16)

s.addTimeLine(ticks=5)