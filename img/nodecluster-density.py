from Drawing import *

s = Drawing(alpha=0, omega=10)

s.addColor("red", "#FF6347")
s.addColor("green", "#32CD32")
s.addColor("blue", "#4169E1")

s.addNode("a")
s.addNode("b")

s.addLink("a", "b", 1, 3, color="blue")
s.addLink("a", "b", 5, 10, color="blue")

s.addNodeCluster("a", [(1,9)], color="red")
s.addNodeCluster("b", [(4,10)], color="green")

s.addTimeLine()