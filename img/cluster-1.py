from Drawing import *

s = Drawing(alpha=0, omega=10)

s.addNode("a")
s.addNode("b")
s.addNode("c")

s.addLink("a", "b", 1, 6)
s.addLink("a", "b", 2, 5, color=10, width=4)
s.addLink("a", "b", 8, 9)
s.addLink("b", "c", 0, 3)
s.addLink("b", "c", 4, 9)
s.addLink("b", "c", 4, 5, color=10, width=4)

s.addNodeCluster("a", [(0,7)], color=11)
s.addNodeCluster("b", [(2,5)], color=11)
s.addNodeCluster("c", [(4,9)], color=11)

s.addTimeLine(ticks=2)