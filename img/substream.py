from Drawing import *

s = Drawing(alpha=0, omega=25)

s.addNode("a")
s.addNode("b")
s.addNode("c")
s.addNode("d")

s.addLink("a","b", 10, 18)

# s.addNodeCluster("a",color=11)
# s.addNodeCluster("c", color=11)
# s.addNodeCluster("d", color=11)

s.addNodeCluster("a", [(5,12)], color=11)
s.addNodeCluster("b", [(5,12)], color=11)
s.addNodeCluster("c", [(5,12)], color=11)
s.addNodeCluster("d", [(5,12)], color=11)

s.addTimeLine(ticks=5)