from Drawing import *

s = Drawing(alpha=0, omega=25)

s.addNode("a")
s.addNode("b")
s.addNode("c")
s.addNode("d")
s.addNode("e")

s.addLink("a", "b", 1, 7)
s.addLink("a", "b", 9, 12)
s.addLink("b", "c", 3, 6)
s.addLink("b", "d", 7, 8, height=0.25)
s.addLink("c", "e", 10, 14, height=0.25)
s.addLink("d", "e", 1, 6)
s.addLink("c","d", 15, 20)
s.addLink("a","c", 16, 20, height=0.25)

s.addNodeCluster("a", [(1,7), (9,12)], color=11)
s.addNodeCluster("c", [(3,6)], color=11)
s.addNodeCluster("d", [(7,8)], color=11)

s.addTimeLine(ticks=5)